//
//  ItemListModal.swift
//  WiproPoc
//
//  Created by Ankit Gupta on 26/04/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

public struct ItemListModal: Codable {
    public var title : String?
    public var rows : [Rows]?
    
}
public class Rows: Codable {
    public let title : String?
    public let description : String?
    public let imageHref : String?
}
